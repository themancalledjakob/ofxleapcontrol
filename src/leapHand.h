//
//  leapHand.h
//  ofxLeapMotion
//
//  Created by Jakob Schlötter on 10/12/13.
//
//
#include "ofMain.h"

#ifndef ofxLeapMotion_leapHand_h
#define ofxLeapMotion_leapHand_h

struct Palm {
    ofVec3f position;
    ofVec3f normal;
    ofVec3f velocity;
};

class leapHand{
    
public:
    ofMesh mesh;
	Palm palm;
    
    leapHand(Palm _palm){
        palm = _palm;
    }
    
    void setPalm(Palm _palm){
        palm = _palm;
    }
    
    void update();
    void draw();
	
    void clear(){
        mesh.clear(); 
    }
};

#endif
